package com.example.softserve.project.controller;

import com.example.softserve.project.model.Employee;
import com.example.softserve.project.service.EmployeeServiceBase;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/employees")
public class UserController {

    @NonNull
    EmployeeServiceBase employeeServiceBase;

    @RequestMapping("")
    public List<Employee> getEmployees() {
        return employeeServiceBase.getAllEmployees();
    }

    @RequestMapping("/{id}")
    public Employee getEmployee(@PathVariable int id) {
        return employeeServiceBase.getEmployee(id);
    }
}
