package com.example.softserve.project.service;

import com.example.softserve.project.model.Employee;

import java.util.List;

public interface EmployeeServiceBase {
    List<Employee> getAllEmployees();
    Employee getEmployee(int id);

}
