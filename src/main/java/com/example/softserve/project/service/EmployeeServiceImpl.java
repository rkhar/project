package com.example.softserve.project.service;

import com.example.softserve.project.repository.EmployeeRepository;
import com.example.softserve.project.model.Employee;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import lombok.NonNull;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeServiceBase {

    @NonNull
    EmployeeRepository employeeRepository;

    @Override
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee getEmployee(int id) {
        return employeeRepository.findById(id).orElse(null);
    }
}
