--liquibase formatted sql

--changeset ruslan:inserting_data

insert into `employee`(first_name,last_name,age,description) values
('Ruslan','Kharevych',19,null),
('Oleg','Senkiv',21,null),
('Taras','Vasilciv',22,'first'),
('Orest','Markiv',29,null),
('Maria','Fedoruk',99,'second'),
('Bogdan','Dydydna',30,null);