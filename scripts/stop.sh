#! /bin/bash

#TODO check resources before removing

docker stop project-mysql
docker stop project-backend

docker rm project-mysql
docker rm project-backend

docker rmi $(docker images | grep  "project*" | awk '{printf "%s:%s", $1, $2}')
docker rmi mysql:5.6
docker rmi openjdk:8-jdk-alpine

docker network rm project-network