#!/bin/bash

BASEDIR=$(dirname "$0")

source ./$BASEDIR/.env

# clear all previous docker artifacts
./$BASEDIR/stop.sh

# creating network to run all related containers in
docker network create project-network

# starting container with mysql DB
docker run --name project-mysql \
    -p :3306 \
    --network project-network \
    -e MYSQL_ROOT_PASSWORD=$DATASOURCE_PASSWORD \
    -e MYSQL_DATABASE=$MYSQL_DATABASE \
    -d mysql:5.6

# building app image
docker build -t project:1.0 .

# starting app container
docker run --name project-backend \
     --env-file ./$BASEDIR/.env \
     --network project-network \
     -p 8080:8080 \
-d project:1.0